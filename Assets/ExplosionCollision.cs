﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionCollision : MonoBehaviour {

	void OnParticleCollision (GameObject other) 
	{
		Debug.Log("entra");
		if (other.tag == "Enemy")
			other.GetComponent<Enemy>().Kill();
		if (other.tag == "Player")
			other.GetComponent<playerController>().Kill();
		if (other.tag == "NPC")
			other.GetComponent<NPC>().Kill();	
	}
}
