using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class dynamicPageGenerator : MonoBehaviour {

	public GameObject vignettePrefab;
	public float y0 =  6.0f;
	public float y1 =  0.0f;
	public float y2 = -6.0f;

	public float xIni = -30.0f;

		float x = -30;
		float y = 0;

	GameObject clonObject;
	
	void Start () {
		GenerateStruc();
	}
	
	void GenerateStruc () {
	
		string sceneName = "";
		
		string[] lines = new string[3];		
		
       foreach (PageObj pageObj in GameManager.gameManager.pageCollection.Pages) {

            if (pageObj.pageName == SceneManager.GetActiveScene().name)
            {
				lines[0]  = pageObj.line0;
				lines[1]  = pageObj.line1;
				lines[2]  = pageObj.line2;
				break;
		    }
        }
		

		for (int j=0; j<3 ; j++)
		{

			if (j==0) y = y0;
       		else if (j==1) y = y1;
       		else if (j==2) y = y2;

       		x = xIni;

			for (int i=0; i<lines[j].Length; i++)           	
        	{
        		if (lines[j].Substring(i, 1) != "#") 
        		{
					sceneName = string.Concat(sceneName+lines[j].Substring(i, 1));
        		} else
        		{
        			//Debug.Log("sceneName= "+sceneName);
        			drawSceneObject(sceneName, y);    	
        			sceneName = "";
        		}

        	}	

        }
    


	}



	void drawSceneObject (string sceneName, float y) {
		        			

		float width = 0;
		float heigth = 0;

		foreach (SceneObj sceneObj in GameManager.gameManager.sceneCollection.Scenes) {

            if (sceneObj.sceneName == sceneName)
            {
				width = float.Parse(sceneObj.width);
				heigth = float.Parse(sceneObj.heigth);
				//Debug.Log("width= "+width+"    heigth= "+heigth);
		        break;
		    }
        }

//Debug.Log("xIni= "+xIni+"   width= "+width);
		x += width/2;
		//Debug.Log("x= "+x);
		clonObject = (GameObject)  Instantiate(vignettePrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject; 	
		clonObject.transform.localScale = new Vector3 (clonObject.transform.localScale.x*width, clonObject.transform.localScale.y*heigth, clonObject.transform.localScale.z);
		clonObject.transform.gameObject.GetComponent<PageButton>().sceneName = sceneName;
		x += width/2+2;

	}
}
