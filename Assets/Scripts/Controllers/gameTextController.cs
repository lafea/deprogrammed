using UnityEngine;
using System.Collections;

public class gameTextController : MonoBehaviour {

    public int gameTextCount;
    public float gameTextMarkOffset = 8.0f;
    public Transform gameTextMark;


    void Start()
    {
        LoadGameText();
        //MoveGameTextMark();
    } 

    void LoadGameText()
    { 

       int localCount = 1;
       foreach (UserGameProgress userGameProgress in GameManager.gameManager.userGameProgressCollection.usersGameProgress) {
            if (localCount==gameTextCount) {
                transform.GetComponent<TextMesh>().text = string.Concat(userGameProgress.gameTextCount, "-", userGameProgress.gameName, "- P", userGameProgress.scene, "-", userGameProgress.saveNow);
                break;
            }
            localCount ++;
        }
    }

    void MoveGameTextMark()
    {  
        gameTextMark.position = new Vector3(transform.position.x-gameTextMarkOffset, transform.position.y, gameTextMark.position.z);
    } 

    void OnMouseDown()
    {  
       int localCount = 1;
       foreach (UserGameProgress userGameProgress in GameManager.gameManager.userGameProgressCollection.usersGameProgress) {
           // Debug.Log("localCoount="+localCount +" / gameTextCount="+gameTextCount  );
            if (localCount==gameTextCount) {
                GameManager.gameManager.currentUserGameProgress = userGameProgress;
                break;
            }
            localCount++;
        }
        MoveGameTextMark();
    }   

}
