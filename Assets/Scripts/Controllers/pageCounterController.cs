using UnityEngine;
using System.Collections;

public class pageCounterController : MonoBehaviour {

    void Start()
    {
        LoadText();
    } 

    void LoadText()
    { 
        if(GameManager.gameManager.currentUserGameProgress!=null)
            transform.GetComponent<TextMesh>().text = GameManager.gameManager.currentUserGameProgress.scene;
    } 
}
