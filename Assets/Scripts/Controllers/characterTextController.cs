using UnityEngine;
using System;
using System.Collections;

public class characterTextController : MonoBehaviour {

  public GameObject character;

    void FixedUpdate()
    {
      if (character!=null)
      {
        transform.position = new Vector3(character.transform.position.x, character.transform.position.y+0.3f, transform.position.z);
      }
      if (transform.GetComponent<TextMesh>().text == "") this.gameObject.SetActive(false);
      else this.gameObject.SetActive(true);
    } 

}
