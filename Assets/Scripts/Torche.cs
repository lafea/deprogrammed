﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torche : MonoBehaviour {

	public Transform torcheLight;

	Animator anim;
	float speed;

	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator>();

		speed = Random.Range(0.9f, 1.1f);

		if (anim.GetCurrentAnimatorStateInfo(0).IsName("torche"))
			anim.speed = speed;
	}
	
	// Update is called once per frame
	void Update () {
		//if (!torcheLight==null)
		torcheLight.transform.position = new Vector3 (torcheLight.transform.position.x+Random.Range(-0.01f, 0.01f)*speed, torcheLight.transform.position.y+Random.Range(-0.01f, 0.01f)*speed, torcheLight.transform.position.z);
		if (Mathf.Abs(torcheLight.transform.position.x-transform.position.x)>0.1 || Mathf.Abs(torcheLight.transform.position.y-transform.position.y)>0.1) 
			torcheLight.transform.position = new Vector3 (transform.position.x, transform.position.y, torcheLight.transform.position.z);
	}
}
