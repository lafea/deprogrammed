using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NPC : MonoBehaviour {

	private INPCState currentState;

    public GameObject characterInfoPrefab;
    GameObject info;


	public float maxSpeedWalk = 5f;
    public bool facingRight = true;
    

    public Animator anim {get; set; }
    
    bool grounded = true;
    public bool isDead = false;
    public Transform groundCheck;
    public float groundRadius = 0.3f;
    public float cellRadius = 1.0f;
    public LayerMask whatIsGround;

    
    float xVel;
    float yVel;

    
    public int lifes = 3;

    void Start()
    {
    	/*info = (GameObject)  Instantiate(characterInfoPrefab, new Vector3 (GetComponent<Transform>().position.x, GetComponent<Transform>().position.y+0.3f, GetComponent<Transform>().position.z-1), Quaternion.identity) as GameObject;
        info.GetComponent<characterTextController>().character = transform.gameObject;*/
        anim = this.GetComponent<Animator>();
        ChangeState(new NPCIdleState());
    }

    public void Kill()
    {
        lifes = 0;
    }

    void Update()
    {   
        isDead = anim.GetBool("isDead");  
        if (!isDead)
        {
    	   currentState.Execute();
           CheckDamage();
        }
    }

    void CheckDamage()
    {
            if (lifes==0)
            {
                ChangeState(new NPCDeadState());
            } 
    }


    public void Flip ()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    

    public void ChangeState (INPCState newState)
    {
        if (currentState != null)
        {
        	currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this);
    }  

    public void Move(){

        grounded = IsGrounded();
        
        if (grounded)
        {
            xVel = anim.GetFloat("xVel");  
            GetComponent<Rigidbody2D>().velocity = new Vector2(xVel*GetDirection().x, 0);
        } 

    } 

    public Vector2 GetDirection()
    {
    	return facingRight ? Vector2.right : Vector2.left;
    }

    void OnTriggerEnter2D(Collider2D other)
    {   
    	if (currentState!=null)
            currentState.OnTriggerEnter(other);
    }

    public bool IsGrounded() {
        return Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
    }

    public void ApplyDamage(int damage)
    {
        if (lifes>0) lifes = lifes - damage;
    }

}
