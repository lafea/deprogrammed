using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public interface INPCState {

	void Execute();
	void Enter(NPC npc);
	void Exit();
	void OnTriggerEnter(Collider2D other);


}