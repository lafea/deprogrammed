using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {

	private IEnemyState currentState;

    public GameObject characterInfoPrefab;
    public GameObject info;


	public GameObject Target  {get; set; }

    public Transform shootTransform;

    public float maxSpeedRotate = 10f;
    public float maxSpeedWalk = 5f;
    float maxSpeedWalk2;
    public bool facingRight = true;
    

    public Animator anim {get; set; }
    
    bool grounded = true;
    
    //bool down = false;
    
    public bool isDead = false;
    public Transform groundCheck;
    public float groundRadius = 0.3f;
    public float cellRadius = 1.0f;
    public LayerMask whatIsGround;
    public float jumpForce = 700f;
    
    float xVel;
    float yVel;

    public AudioSource jumpSound;
    public AudioSource runSound;

    public GameObject shootPrefab;
   // public float shootTime = 2;

    public int lifes = 3;

    public bool isVoid = false;
    public int extraAmmo;
    public int extraAmmoMin = 0;
    public int extraAmmoMax = 20;

    public LayerMask layerPlayer;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        info = (GameObject)  Instantiate(characterInfoPrefab, new Vector3 (GetComponent<Transform>().position.x, GetComponent<Transform>().position.y+0.3f, GetComponent<Transform>().position.z-1), Quaternion.identity) as GameObject;
        info.GetComponent<characterTextController>().character = transform.gameObject;
        ChangeState(new IdleState());
        extraAmmo = Random.Range(extraAmmoMin, extraAmmoMax);
        if (extraAmmo == 0) isVoid = true;
        info.transform.GetComponent<TextMesh>().text = string.Concat(lifes);
    }

    public void Kill()
    {
        lifes = 0;
    }

    void Update()
    {   
        isDead = anim.GetBool("isDead");  
        if (!isDead)
        {
    	   currentState.Execute();
    	   LookAtTarget();
           CheckDamage();
        }

    }

    void LookAtTarget()
    {
    	if (Target != null)
    	{
    		float xDir = Target.transform.position.x - transform.position.x;

    		if (xDir < 0 && facingRight || xDir >= 0 && !facingRight ) 
    		{
    			ChangeState(new RotateState());
    			Invoke("Flip", 1);
    		}
    	}
    }

    void CheckDamage()
    {
            if (lifes==0)
            {
                ChangeState(new DeadState());
            } 
    }


    public void Flip ()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Shoot ()
    {
        #pragma warning disable 0219
        int xOff;
        #pragma warning restore 0219

        if (facingRight) xOff = 2;
        else xOff = -2;
        GameObject shoot = Instantiate(shootPrefab, new Vector3(shootTransform.position.x, shootTransform.position.y, shootTransform.position.z), Quaternion.identity) as GameObject;
        shoot.GetComponent<shootController>().facingRight = facingRight;
        if (!facingRight) shoot.transform.localScale *= -1;
        hit();
    }

    public void hit()
    {
     
        Vector3 targetPosition;
        if (facingRight) targetPosition = new Vector3 (transform.position.x+4, transform.position.y, transform.position.z);
        else             targetPosition = new Vector3 (transform.position.x-4, transform.position.y, transform.position.z);

        //Debug.DrawRay(transform.position, targetPosition - transform.position, Color.red); //debug ray to see the ray
     
        RaycastHit2D hit = Physics2D.Raycast(transform.position, targetPosition - transform.position,
            Vector2.Distance(targetPosition, transform.position), layerPlayer);//layer 12 = Player from my transform in the direction to the player as seen in the debug ray
     
        if (hit.collider != null)//if the object that was hit goes by the name "Player" then:
        {
            //
            hit.transform.gameObject.GetComponent<playerController>().ApplyDamage(1);  
        }
    }

    public void ChangeState (IEnemyState newState)
    {
        if (currentState != null)
        {
        	currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this);
    }  

    public void Move(){

        grounded = IsGrounded();
        
        if (grounded)
        {
            xVel = anim.GetFloat("xVel");  
            GetComponent<Rigidbody2D>().velocity = new Vector2(xVel*GetDirection().x, 0);
        } 

    } 

    public Vector2 GetDirection()
    {
    	return facingRight ? Vector2.right : Vector2.left;
    }

    void OnTriggerEnter2D(Collider2D other)
    {   
    	if (currentState!=null)
            currentState.OnTriggerEnter(other);
    }

    public void Jump(){ 
        if (grounded)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(GetDirection().x*jumpForce, jumpForce));
        }
    }

    public bool IsGrounded() {
        return Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
    }

    public void ApplyDamage(int damage)
    {
        if (lifes>0) 
        {
            lifes = lifes - damage;
            info.transform.GetComponent<TextMesh>().text = string.Concat(lifes);
        } else if (!isVoid) info.transform.GetComponent<TextMesh>().text = string.Concat("+");
        else info.transform.GetComponent<TextMesh>().text = "";
    }

}
