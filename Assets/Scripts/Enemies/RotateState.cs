using UnityEngine;
using System.Collections;

public class RotateState : IEnemyState {

	private Enemy enemy;
	private float rotateTimer;
	private float rotateDuration = 0.2f;

    public void Execute(){
    	Rotate();
        enemy.Move();
        if (enemy.Target != null)
        {
            enemy.ChangeState(new PatrolState());
        }
    }

    public void Enter(Enemy enemy){
    		this.enemy = enemy;

    }

    public void Exit(){


    }

    public void OnTriggerEnter(Collider2D other){
        if (other.tag == "FlipEdge")
        {
            enemy.Flip();
        }
    }

    void Rotate()
    {
    	enemy.anim.SetFloat("xVel", enemy.GetComponent<Enemy>().maxSpeedRotate);
    	enemy.anim.SetBool("isGrounded", true);
    	enemy.anim.SetBool("isDown", true);

    	rotateTimer += Time.deltaTime;

    	if (rotateTimer >= rotateDuration)
    	{
    		enemy.ChangeState(new RangedState());
    	}
    }


}
