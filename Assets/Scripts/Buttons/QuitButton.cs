﻿using UnityEngine;
using System.Collections;

public class QuitButton : MonoBehaviour {
	
	void OnMouseDown()
    {    
            GetComponent<AudioSource>().Play();
            Application.Quit();   
    }
}
