﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PageButton : MonoBehaviour {

	public string sceneName;
	
	void OnMouseDown()
    {    
            SceneManager.LoadScene(sceneName);
            GetComponent<AudioSource>().Play();
    }
}
