﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GamePlayButton : MonoBehaviour
{

    public bool saveNewGame;

    void OnMouseDown()
    {  
        if (saveNewGame) SaveNewGame();
        GameManager.gameManager.pause = false;
        SceneManager.LoadScene(GameManager.gameManager.currentUserGameProgress.scene);
        GetComponent<AudioSource>().Play();
        //GameManager.gameManager.notSavedGameProgress = GameManager.gameManager.currentUserGameProgress;
    }   


    void SaveNewGame()
    {  
        DateTime saveNow = DateTime.Now;
        string datePatt = @"M/d/yyyy hh:mm:ss tt";

        UserGameProgress userGameProgress = new UserGameProgress();
        userGameProgress.gameName = GameManager.gameManager.newGameName;
        userGameProgress.saveNow = saveNow.ToString(datePatt);
        userGameProgress.scene = "1";
        userGameProgress.health = "100";
        userGameProgress.colors = "000";
        
        GameManager.gameManager.userGameProgressCollection.usersGameProgress.Add(userGameProgress);
        GameManager.gameManager.SaveUserGameProgress();

        GameManager.gameManager.currentUserGameProgress = userGameProgress;
        GameManager.gameManager.notSavedGameProgress = userGameProgress;

    } 
}
