﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{

	public string scene;

    void OnMouseDown()
    {  
            SceneManager.LoadScene(scene);
            GetComponent<AudioSource>().Play();
    }    
}
