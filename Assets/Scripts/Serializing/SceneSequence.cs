using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
 
[XmlRoot("SceneSequence")]
public class SceneSequence
{
    [XmlArray("SceneSequences")]
  
    public SceneSeq[] SceneSequences;
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneSequence));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
   
    public static SceneSequence Load(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneSequence));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as SceneSequence;
        }
    }
}