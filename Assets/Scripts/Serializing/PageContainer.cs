using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
 
[XmlRoot("PageCollection")]
public class PageContainer
{
    [XmlArray("Pages")]
  
    public PageObj[] Pages;
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(PageContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
   
    public static PageContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(PageContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as PageContainer;
        }
    }
}