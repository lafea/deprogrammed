using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
 
[XmlRoot("UserPreferencesCollection")]
public class UserGameProgressContainer
{
    [XmlArray("UsersGamesProgress")]
    
    public List<UserGameProgress> usersGameProgress = new List<UserGameProgress>();
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(UserGameProgressContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            serializer.Serialize(xmlWriter, this);
        }
    }
   
    public static UserGameProgressContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(UserGameProgressContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as UserGameProgressContainer;
        }
    }
}