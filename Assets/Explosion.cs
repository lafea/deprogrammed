﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public GameObject explosion;
	GameObject expl;
	public float explDuration = 0.1f;

	public void Explote () {
		expl = (GameObject) Instantiate(explosion, transform.position, Quaternion.identity) as GameObject; 
		Destroy(expl, explDuration);
		Destroy(gameObject);

	}
}
